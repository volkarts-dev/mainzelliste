package de.pseudonymisierung.mainzelliste.crypto;

import de.pseudonymisierung.mainzelliste.crypto.key.CryptoKey;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TinkDeterministicAeadCipherTest {

  final static private String CRYPTO_KEY_FOLDER = "./ci/newman_mainzelliste_resources/keys/";

  @Test
  public void test_encrypt() throws GeneralSecurityException, IOException {
    //prepare keys and encryption
    CryptoKey tinkSymmetricKey = CryptoUtil.readKey("TinkKeySet",
        Files.readAllBytes(new File(CRYPTO_KEY_FOLDER + "symmetric_key.der").toPath()));
    SymmetricCipher symmetricCipher = SymmetricCipher.getInstance(
        TinkDeterministicAeadCipher.class, tinkSymmetricKey);

    // test encryption
    String plainText = "Test Text";
    String base64CipherText = symmetricCipher.encryptToBase64String(plainText);

    // try to decrypt
    String decryptedText = symmetricCipher.decryptFromBase64String(base64CipherText);

    Assert.assertEquals(decryptedText, plainText);
  }
}
