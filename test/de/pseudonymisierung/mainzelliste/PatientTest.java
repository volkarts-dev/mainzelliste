package de.pseudonymisierung.mainzelliste;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PatientTest {

  Patient originalPatientA = createPatientA(); // "Max", "Mustermann", null

  @DataProvider(name = "patient")
  public Object[][] createPatient() {
    Patient p = new Patient();
    Map<String, Field<?>> pat_input = new HashMap<String, Field<?>>();
    pat_input.put("vorname", new PlainTextField("Marcel"));
    pat_input.put("nachname", new PlainTextField("Parciak"));
    p.setFields(pat_input);
    return new Object[][]{
        {p}
    };
  }

  protected AssociatedIds createAssocId(String extType, String extString, String intType,
      String intString) {
    AssociatedIds assocId = new AssociatedIds("faelle");

    ExternalID extId = new ExternalID();
    extId.setType(extType);
    extId.setIdString(extString);
    IntegerID intId = new IntegerID();
    intId.setType(intType);
    intId.setIdString(intString);

    assocId.addId(extId);
    assocId.addId(intId);

    return assocId;
  }

  @Test(dataProvider = "patient")
  public void baseAssumptions(Patient patient) {
    Assert.assertNotNull(patient.getAssociatedIdsList());
    Assert.assertEquals(patient.getAssociatedIdsList().size(), 0);
  }

  @Test(dataProvider = "patient")
  public void addAssociatedIdentifier(Patient patient) {
    AssociatedIds assocId = this.createAssocId("extVisitId", "asdf1234", "fall_psn", "1");
    Assert.assertNotNull(patient.getAssociatedIdsList());
    Assert.assertEquals(patient.getAssociatedIdsList().size(), 0);

    patient.associatedIdsList.add(assocId);
    Assert.assertEquals(patient.getAssociatedIdsList().get(0), assocId);
    Assert.assertEquals(patient.getAssociatedIdsList().size(), 1);
  }

  @Test(dataProvider = "patient")
  public void addAssociatedIdentifiers(Patient patient) {
    AssociatedIds assocId_1 = this.createAssocId("extVisitId", "asdf1234", "fall_psn", "1");
    patient.associatedIdsList.add(assocId_1);
    AssociatedIds assocId_2 = this.createAssocId("extVisitId", "fdsa4321", "fall_psn", "2");
    patient.associatedIdsList.add(assocId_2);

    Assert.assertEquals(patient.getAssociatedIdsList().size(), 2);
    Assert.assertTrue(patient.getAssociatedIdsList().contains(assocId_1));
    Assert.assertTrue(patient.getAssociatedIdsList().contains(assocId_2));
  }

  @Test(dataProvider = "patient")
  public void getAssociatedIdentifiers(Patient patient) {
    AssociatedIds assocId_1 = this.createAssocId("extVisitId", "asdf1234", "fall_psn", "1");
    patient.associatedIdsList.add(assocId_1);
    AssociatedIds assocId_2 = this.createAssocId("extVisitId", "fdsa4321", "fall_psn", "2");
    patient.associatedIdsList.add(assocId_2);

    Assert.assertEquals(patient.associatedIdsList.get(0), assocId_1);
    Assert.assertTrue(patient.getAssociatedIdsList().contains(assocId_1));

    Assert.assertEquals(patient.associatedIdsList.get(1), assocId_2);
    Assert.assertTrue(patient.getAssociatedIdsList().contains(assocId_2));
  }

  /**
   * T1: update patient with the same fields
   */
  @Test
  public void testUpdateFrom_samePatient() {
    Patient patientA = createPatientA(); // "Max", "Mustermann"
    Patient patientB = createPatient("Max", "Mustermann", null);
    // execute test
    patientA.updateFrom(patientB, Collections.emptySet(), Collections.emptyList());
    // fields of patientA should remain unchanged
    for (Entry<String, Field<?>> entry : originalPatientA.getFields().entrySet()) {
      Assert.assertEquals(patientA.getFields().get(entry.getKey()), entry.getValue());
    }
    Assert.assertEquals(patientA.getFields().size(), patientB.getFields().size());
  }

  /**
   * T2: update patient with a different field
   */
  @Test
  public void testUpdateFrom_differentFieldValues() {
    Patient patientA = createPatientA(); // "Max", "Mustermann"
    Patient patientB = createPatient("Max", "Peter", null);
    // execute test
    patientA.updateFrom(patientB, Collections.emptySet(), Collections.emptyList());
    // fields of patientA should remain unchanged
    for (Entry<String, Field<?>> entry : originalPatientA.getFields().entrySet()) {
      Assert.assertEquals(patientA.getFields().get(entry.getKey()), entry.getValue());
    }
    Assert.assertEquals(patientA.getFields().size(), patientB.getFields().size());
  }

  /**
   * T3: update patient with new field
   */
  //TODO refactor the initialization of Config class
//  @Test
//  public void testUpdateFrom_addNewField() {
//    Patient patientA = createPatientA(); // "Max", "Mustermann"
//    Patient patientB = createPatient("Max", "Mustermann", "01.02.1912");
//    // execute test
//    patientA.updateFrom(patientB, Collections.emptySet(), Collections.emptyList());
//    // fields of patientA should remain unchanged
//    for (Entry<String, Field<?>> entry : originalPatientA.getFields().entrySet()) {
//      Assert.assertEquals(patientA.getFields().get(entry.getKey()), entry.getValue());
//    }
//    // new field should be add
//    Assert.assertEquals(patientA.getFields().get("birthday").getValue(), "01.02.1912");
//    Assert.assertEquals(patientA.getFields().size(), patientB.getFields().size());
//  }

  /**
   * T4: update patient with missed field
   */
  @Test
  public void testUpdateFrom_missedField() {
    Patient patientA = createPatientA(); // "Max", "Mustermann", null
    Patient patientB = createPatient("Max", null, null);
    // execute test
    patientA.updateFrom(patientB, Collections.emptySet(), Collections.emptyList());
    // fields of patientA should remain unchanged
    for (Entry<String, Field<?>> entry : originalPatientA.getFields().entrySet()) {
      Assert.assertEquals(patientA.getFields().get(entry.getKey()), entry.getValue());
    }
    Assert.assertEquals(patientA.getFields().size(), originalPatientA.getFields().size());
  }

  // Utils
  ///////////////

  private Patient createPatientA() {
    return createPatient("Max", "Mustermann", null);
  }

  private Patient createPatient(String name, String lastname, String birthday) {
    Patient patient = new Patient();
    Map<String, Field<?>> fields = new HashMap<>();
    if (name != null) {
      fields.put("name", new PlainTextField(name));
    }
    if (lastname != null) {
      fields.put("lastname", new PlainTextField(lastname));
    }
    if (birthday != null) {
      fields.put("birthday", new PlainTextField(birthday));
    }
    patient.setFields(fields);
    return patient;
  }
}
