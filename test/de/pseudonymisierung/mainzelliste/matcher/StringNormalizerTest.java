package de.pseudonymisierung.mainzelliste.matcher;

import de.pseudonymisierung.mainzelliste.PlainTextField;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class StringNormalizerTest {

  @DataProvider
  public Object[][] sendData(){
    return new Object[][]{
        {" .:,;-'äxÄxöxÖx .:,;-xüxÜxßxéxÉ .:,;-'", "AEXAEXOEXOEX .:,;-XUEXUEXSSXEXE"}
    };
  }

  @Test(dataProvider="sendData")
  public void testTransform(String inputString, String resultString){
    PlainTextField input = new PlainTextField(inputString);
    PlainTextField expectedResult = new PlainTextField(resultString);
    PlainTextField result = new StringNormalizer().transform(input);
    Assert.assertEquals(result.getValue(), expectedResult.getValue());
  }

}