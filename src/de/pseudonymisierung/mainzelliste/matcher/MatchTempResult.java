package de.pseudonymisierung.mainzelliste.matcher;

import de.pseudonymisierung.mainzelliste.Patient;
import java.util.List;
import java.util.NavigableMap;
import java.util.TreeMap;

public class MatchTempResult {

    double bestWeight = Double.NEGATIVE_INFINITY;
    NavigableMap<Double, List<Patient>> possibleMatches = new TreeMap<>();
    Patient bestMatch = null;

    public MatchTempResult() {
    }

    public MatchTempResult(double bestWeight, Patient bestMatch) {
        this.bestWeight = bestWeight;
        this.bestMatch = bestMatch;
    }

    public Patient getBestMatch() {
        return bestMatch;
    }

    public void setBestMatch(Patient bestMatch) {
        this.bestMatch = bestMatch;
    }

    public double getBestWeight() {
        return bestWeight;
    }

    public void setBestWeight(double bestWeight) {
        this.bestWeight = bestWeight;
    }

    public NavigableMap<Double, List<Patient>> getPossibleMatches() {
        return possibleMatches;
    }

    public void setPossibleMatches(NavigableMap<Double, List<Patient>> possibleMatches) {
        this.possibleMatches = possibleMatches;
    }
}