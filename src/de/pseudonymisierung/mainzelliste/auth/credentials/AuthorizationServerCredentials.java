package de.pseudonymisierung.mainzelliste.auth.credentials;

public interface AuthorizationServerCredentials {

  String getServerId();

}
