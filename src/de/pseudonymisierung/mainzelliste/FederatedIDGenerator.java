/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.DataFormatException;
import ca.uhn.fhir.parser.IParser;
import de.pseudonymisierung.mainzelliste.client.fttp.bloomfilter.RecordBloomFilter;
import de.pseudonymisierung.mainzelliste.client.fttp.bloomfilter.RecordBloomFilterGenerator;
import de.pseudonymisierung.mainzelliste.exceptions.InternalErrorException;
import de.pseudonymisierung.mainzelliste.exceptions.InvalidConfigurationException;
import de.pseudonymisierung.mainzelliste.exceptions.NotImplementedException;
import de.pseudonymisierung.mainzelliste.util.ConfigUtils;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hl7.fhir.r4.model.Base64BinaryType;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Parameters;

/**
 * Pseudo generator for externally generated patient identifiers.
 */
public class FederatedIDGenerator implements IDGenerator<FederatedID> {

  private static final Logger logger = LogManager.getLogger(FederatedIDGenerator.class);

  String idType;
  /**
   * list of configured ID types with which this generator will create the ID eagerly
   */
  private List<String> eagerGenRelatedIdTypes;

  private final Map<String, String> requestInputData = new HashMap<>();

  private RecordBloomFilterGenerator bloomFilterGenerator;

  private CloseableHttpClient client;

  private URI url;

  private String apiKey;

  private static final IParser parser = FhirContext.forR4().newXmlParser();

  @Override
  public void init(IDGeneratorMemory mem, String idType, String[] eagerGenRelatedIdTypes,
      Properties props) {
    this.idType = idType;
    this.eagerGenRelatedIdTypes = Arrays.asList(eagerGenRelatedIdTypes);
    ConfigUtils.getVariableSubProperties(props, "request").forEach((n, p) ->
        p.forEach((key, value) -> requestInputData.put((String) key, (String) value))
    );
    this.bloomFilterGenerator = Config.instance
        .getRecordBloomFilterGenerator(requestInputData.get("recordBloomFilter"));
    HttpHost proxy = null;
    try {
      String urlString = props.getProperty("fttp.url", "").trim();
      if (urlString.isEmpty()) {
        throw new InvalidConfigurationException("idgenerator." + idType + ".fttp.url not configured");
      }
      this.url = new URL(urlString).toURI();
      proxy = Config.instance.getProxyUrl(this.url.toString());
    } catch (MalformedURLException | URISyntaxException e) {
      throw new InvalidConfigurationException("idgenerator." + idType + ".fttp.url is invalid",
          e.getMessage());
    }
    this.apiKey = props.getProperty("fttp.authentication.apikey", "").trim();

    //init http client
    SSLContext sslContext;
    try {
      KeyStore keyStore = KeyStore.getInstance(props
          .getProperty("fttp.authentication.certificat.keystore.name", "").trim());
      keyStore.load(new FileInputStream(props
          .getProperty("fttp.authentication.certificat.file", "").trim()), props
          .getProperty("fttp.authentication.certificat.password", "").trim().toCharArray());

      KeyManagerFactory keyManagerFactory = KeyManagerFactory
          .getInstance(KeyManagerFactory.getDefaultAlgorithm());
      keyManagerFactory.init(keyStore, props
          .getProperty("fttp.authentication.certificat.keystore.password", "").trim()
          .toCharArray());
      sslContext = SSLContext.getInstance("TLS");
      sslContext.init(keyManagerFactory.getKeyManagers(), null, null);
    } catch (KeyStoreException e) {
      throw new InvalidConfigurationException(
          "idgenerator." + idType + ".fttp.authentication.certificat.keystore.name", props
          .getProperty("authentication.certificat.keystore.name"), e);
    } catch (IOException | CertificateException e) {
      throw new InvalidConfigurationException(
          "idgenerator." + idType + ".fttp.authentication.certificat.keystore.name", props
          .getProperty("authentication.certificat.file"), e);
    } catch (UnrecoverableKeyException | NoSuchAlgorithmException | KeyManagementException e) {
      throw new InvalidConfigurationException("idgenerator." + idType + ".fttp.authentication.*",
          e);
    }

    HttpClientBuilder httpClientBuilder = HttpClients.custom();
    httpClientBuilder.setSSLSocketFactory(new SSLConnectionSocketFactory(sslContext,
        new String[]{"TLSv1", "TLSv1.1", "TLSv1.2"}, null,
        SSLConnectionSocketFactory.getDefaultHostnameVerifier())).build();
    //set proxy
    if (proxy != null) {
      httpClientBuilder.setRoutePlanner(new DefaultProxyRoutePlanner(proxy));
    }
    httpClientBuilder.setRedirectStrategy(new LaxRedirectStrategy());
    client = httpClientBuilder.build();
  }

  @Override
  public boolean verify(String id) {
    return true;
  }

  @Override
  public synchronized FederatedID getNext() {
    throw new NotImplementedException("Can't get next ID for federated id type");
  }

  public synchronized FederatedID getId(Patient patient) {
    //build request
    HttpPost request = new HttpPost(this.url);
    parser.setPrettyPrint(true);
    Parameters parameters = new Parameters();
    parameters.addParameter("study", requestInputData.get("study"));
    RecordBloomFilter bloomFilter = bloomFilterGenerator.generateBalancedBloomFilter(
        patient.getRecordBloomFilter(requestInputData.get("recordBloomFilter")));
    String base64BloomFilter = bloomFilter.getBase64String();
    logger.debug("generated bloom filter: {}", base64BloomFilter);
    parameters.addParameter("bloomfilter", new Base64BinaryType(base64BloomFilter));
    parameters.addParameter("target", requestInputData.get("target"));
    parameters.addParameter("apikey", apiKey);
    StringEntity httpEntity = new StringEntity(parser.encodeResourceToString(parameters),
        ContentType.create("application/fhir+xml", StandardCharsets.UTF_8));
    request.setEntity(httpEntity);

    // send request
    try (CloseableHttpResponse response = client.execute(request)) {
      String entityString = EntityUtils.toString(response.getEntity());
      int statusCode = response.getStatusLine().getStatusCode();
      logger.debug("Generate federated id \"{}\". returned status code : {} and entity: {}",
          idType, statusCode, entityString);
      try {
        Parameters returnedParameters = parser.parseResource(Parameters.class, entityString);
        if (statusCode == 200) {
          String idString = ((Identifier) returnedParameters.getParameter().stream()
              .filter(p -> p.getName().equals("pseudonym-bf"))
              .findFirst().get().getPart().stream()
              .filter(r -> r.getName().equals("pseudonym")).findFirst().get().getValue())
              .getValue();
          return new FederatedID(idString, idType);
        } else {
          logger.error("Can't generate federated id {}! returned status code [{}]", idType,
              statusCode);
          String erroMessage = ((Coding) returnedParameters.getParameter().stream()
              .filter(p -> p.getName().equals("error"))
              .findFirst().get().getPart().stream()
              .filter(r -> r.getName().equals("error-code")).findFirst().get().getValue())
              .getDisplay();
          throw new InternalErrorException("can't generate federated id " + idType
              + ". returned cause : " + statusCode + " " + erroMessage);
        }
      } catch (DataFormatException e) {
        String errorMsg = "Can't generate federated id " + idType
            + "! Invalid data format. Cause: [" + e.getMessage() + "]. returned status code ["
            + statusCode + "], returned message [" + entityString + "]";
        logger.error(errorMsg);
        throw new InternalErrorException(errorMsg);
      }
    } catch (IOException e) {
      throw new InternalErrorException("Can't generate federated id " + idType +
          ". Cause : " + e.getMessage());
    }
  }

  /**
   * Not implemented for federated IDs.
   *
   * @throws NotImplementedException
   */
  @Override
  public String correct(String idString) {
    throw new NotImplementedException("Cannot correct federated ID!");
  }

  @Override
  public FederatedID buildId(String id) {
    return new FederatedID(id, getIdType());
  }

  @Override
  public String getIdType() {
    return idType;
  }

  @Override
  public boolean isExternal() {
    return false;
  }

  @Override
  public boolean isPersistent() {
    return true;
  }

  @Override
  public Optional<IDGeneratorMemory> getMemory() {
    return Optional.empty();
  }

  @Override
  public boolean isEagerGenerationOn(String idType) {
    return eagerGenRelatedIdTypes.contains("*") || eagerGenRelatedIdTypes.contains(idType);
  }
}
