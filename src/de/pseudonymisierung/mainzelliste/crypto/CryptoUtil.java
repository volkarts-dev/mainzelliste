/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste.crypto;

import de.pseudonymisierung.mainzelliste.crypto.key.CryptoKey;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.security.spec.InvalidKeySpecException;

public class CryptoUtil {

  private static final String CRYPTO_CLASS_PATH = "de.pseudonymisierung.mainzelliste.crypto.";
  private static final String CRYPTO_KEY_CLASS_PATH = "de.pseudonymisierung.mainzelliste.crypto.key.";

  private CryptoUtil() {
    throw new IllegalStateException("Utility class");
  }

  public static Class<?> findCipherClass(String cipherType) {
    try {
      return Class.forName(CRYPTO_CLASS_PATH + cipherType);
    } catch (ClassNotFoundException e) {
      throw new IllegalArgumentException("the given cipher type " + cipherType + " not found");
    } catch (SecurityException e) {
      throw new IllegalArgumentException("the given cipher type " + cipherType
          + " can't be initialized. ", e);
    }
  }

  protected static <C> C createCipher(Class<?> cipherClass, CryptoKey wrappedKey, Class<C> cipherType)
      throws InvalidKeySpecException {
    try {
      Constructor<C> constructor = ((Class<C>)cipherClass).getConstructor(CryptoKey.class);
      return constructor.newInstance(wrappedKey);
    } catch (InvocationTargetException e) {
      Throwable targetException = e.getTargetException();
      if (targetException instanceof IllegalArgumentException) {
        throw new InvalidKeySpecException("The given crypto key '" +
            wrappedKey.getKey().getClass().getSimpleName() + "' and the cipher type '" +
            cipherClass.getSimpleName() + "' are incompatible ");
      } else if (targetException instanceof InvalidKeySpecException) {
        throw (InvalidKeySpecException) targetException;
      } else {
        throw new UnsupportedOperationException(
            "can't create " + cipherClass.getSimpleName() + " cipher instance", targetException);
      }
    } catch (ReflectiveOperationException | SecurityException e) {
      throw new IllegalArgumentException(
          "the given cipher type " + cipherClass.getSimpleName() + " can't be initialized. ", e);
    }
  }

  public static CryptoKey readKey(String keyType, byte[] encodedKey) {
    try {
      Class<CryptoKey> keyClass = (Class<CryptoKey>) Class.forName(CRYPTO_KEY_CLASS_PATH + keyType);
      Constructor<CryptoKey> constructor = keyClass.getConstructor(byte[].class);
      return constructor.newInstance(encodedKey);
    } catch (ClassNotFoundException e) {
      throw new IllegalArgumentException("the given key type " + keyType + " not found");
    } catch (InvocationTargetException e) {
      throw new UnsupportedOperationException(e.getTargetException());
    } catch (ReflectiveOperationException | SecurityException e) {
      throw new IllegalArgumentException(
          "the given key type " + keyType + " can't be initialized. ", e);
    }
  }
}
